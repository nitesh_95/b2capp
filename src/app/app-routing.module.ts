import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { RegistrationComponent } from './admin/registration/registration.component';
import { ProductdetailsComponent } from './admin/productdetails/productdetails.component';



const appRoutes: Routes = [
  { path: '', redirectTo: "registration", pathMatch: "full" },
  { path: 'registration', component: RegistrationComponent },
  { path: 'product', component: ProductdetailsComponent },

];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes, { useHash: true })
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
