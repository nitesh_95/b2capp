// import * as $ from 'jquery';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormControlName } from '@angular/forms';
import { Component, OnInit, ViewChildren, QueryList, ElementRef } from '@angular/core';
import { AuthPassService } from 'src/app/service/auth-pass.service';
import { ViewApplicationService } from 'src/app/service/view-application.service';
import { parse } from 'querystring';
import { DatePipe } from '@angular/common';
import { compileComponentFromRender2 } from '@angular/compiler/src/render3/view/compiler';
import { TranslateService } from '@ngx-translate/core';
declare var $: any;
import ConfettiGenerator from "confetti-js";
// declare var $: any;

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  selectedProduct: string = '';
  selectedProducts: string = '';
  value: any
  productId: any
  relationship: any
  toggleBtn: any

  supportLanguages = ['English', 'hindi', 'telugu'];
  constructor(private http: HttpClient,
    private countService: AuthPassService,
    private viewApplication: ViewApplicationService,
    private router: Router,
    private translateService: TranslateService) {
    this.translateService.addLangs(this.supportLanguages);
    this.translateService.setDefaultLang('English');
  }
  responses: any
  placeHolder1: number
  placeHolder2: any
  true: boolean = false;
  rateofInterest: boolean = false
  groupValue: any
  dropDownValues: any
  fatherHusbandName: any
  otpValidationsResent: boolean = false;
  product: any
  firstName: any
  businessname: any
  businessAddress: any
  pan: any
  id: any
  panno: any
  dob: any
  lastName: any
  gender: any
  loanAmount: any
  roi: any
  productIds: any
  noOfMonths: any
  emi: any
  mobileNo: any
  emiCount: any
  businessPinCode: any
  productTypes: any = []
  productTyp: any = []
  responseData: any = []
  tapHereDetails: any = []
  checkLists: any = []
  p: number = 1;
  fstName: any
  lstName: any
  mobNo: any
  dobs: any
  business
  samruddhi: boolean = false
  samrruddhi: boolean = false
  buttonDisabled: boolean = true;
  buttonDisabledd: boolean = false;
  gndr: any
  prodIds: any
  dropDownValuess: any
  dateOfBirth: any
  businessaddress: any
  loading: boolean;
  congratsCanvas: boolean = false
  otp: any
  prodId: any
  otpValidations: boolean = false;
  otpValidation: boolean = false;
  response: any
  status: any
  states: any
  cities: any
  village: any
  updateProducts: any = []
  public pageSize: number = 5;
  next_step = true;
  value1: any
  panNumber:any;
  name1: any
  language: string = 'English'
  textType: boolean = false
  radioType: boolean = false
  questionslist: any = []
  state: any
  district: any
  villageList: any = []
  loan_amount: any
  No_of_months: any
  selectedOption: any;
  form = new FormGroup({
    form1: new FormGroup({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      mobileNumber: new FormControl('', [Validators.required, Validators.minLength(10)]),
      pincode: new FormControl('', [Validators.required, Validators.minLength(6)]),
      gender: new FormControl(null, Validators.required),
      cibilCheckBox: new FormControl(false, Validators.requiredTrue),
    }),
    form2: new FormGroup({
      product: new FormControl(null, Validators.required),
      loanamount: new FormControl('', Validators.required),
      tenure: new FormControl(0, Validators.required),
    }),
    form3: new FormGroup({
      father_husband_name: new FormControl('', Validators.required),
      relationship: new FormControl(null, Validators.required),
      village: new FormControl(null, Validators.required),
      pan: new FormControl('', [Validators.required, Validators.minLength(10)]),
      businessAddress: new FormControl('', Validators.required),

    })
  })
  ngOnInit() {

    console.log(this.language)
    this.samrruddhi = $("#loanAmount").val('');

    function scroll_to_class(element_class, removed_height) {
      var scroll_to = $(element_class).offset().top - removed_height;
      if ($(window).scrollTop() != scroll_to) {
        $('.form-wizard').stop().animate({ scrollTop: scroll_to }, 0);
      }
    }



    function bar_progress(progress_line_object, direction) {
      var number_of_steps = progress_line_object.data('number-of-steps');
      var now_value = progress_line_object.data('now-value');
      var new_value = 0;
      if (direction == 'right') {
        new_value = now_value + (100 / number_of_steps);
      }
      else if (direction == 'left') {
        new_value = now_value - (100 / number_of_steps);
      }
      progress_line_object.attr('style', 'width: ' + new_value + '%;').data('now-value', new_value);
    }

    /*
      Form
    */
    $('.form-wizard fieldset:first').fadeIn('slow');

    $('.form-wizard .required').on('focus', function () {
      $(this).removeClass('input-error');
    });

    // next step
    $('.form-wizard .btn-next').on('click', function () {
      var parent_fieldset = $(this).parents('fieldset');
      var next_step = true;
      // navigation steps / progress steps
      var current_active_step = $(this).parents('.form-wizard').find('.form-wizard-step.active');
      var progress_line = $(this).parents('.form-wizard').find('.form-wizard-progress-line');

      // fields validation
      parent_fieldset.find('.required').each(function () {
        if ($(this).val() == "") {
          $(this).addClass('input-error');
          next_step = false;
        }
        else {
          $(this).removeClass('input-error');
        }
      });
      // fields validation

      if (next_step) {
        parent_fieldset.fadeOut(400, function () {
          // change icons
          current_active_step.removeClass('active').addClass('activated').next().addClass('active');
          // progress bar
          bar_progress(progress_line, 'right');
          // show next step
          $(this).next().fadeIn();
          // scroll window to beginning of the form
          scroll_to_class($('.form-wizard'), 20);
        });
      }

    });

    // previous step
    $('.form-wizard .btn-previous').on('click', function () {
      // navigation steps / progress steps
      var current_active_step = $(this).parents('.form-wizard').find('.form-wizard-step.active');
      var progress_line = $(this).parents('.form-wizard').find('.form-wizard-progress-line');

      $(this).parents('fieldset').fadeOut(400, function () {
        // change icons
        current_active_step.removeClass('active').prev().removeClass('activated').addClass('active');
        // progress bar
        bar_progress(progress_line, 'left');
        // show previous step
        $(this).prev().fadeIn();
        // scroll window to beginning of the form
        scroll_to_class($('.form-wizard'), 20);
      });
    });

    // submit
    $('.form-wizard').on('submit', function (e) {

      // fields validation
      $(this).find('.required').each(function () {
        if ($(this).val() == "") {
          e.preventDefault();
          $(this).addClass('input-error');
        }
        else {
          $(this).removeClass('input-error');
        }
      });
    });

  }

  selectLang(lang: string) {
    this.translateService.use(lang)
    this.language = lang
    console.log(this.language)

  }
  onbtn(event) {
    $(event.target).removeClass('active')
    $(event.target.nextSibling).addClass('active')
  }
  offbtn(event) {
    $(event.target).removeClass('active')
    $(event.target.previousSibling).addClass('active')
  }
  getToday(): string {
    return new Date().toISOString().split('T')[0]
  }
  notinterested(event) {
        $('#notInterested').modal('toggle')
  }
  notinteresteds(event) {
    const base_URL = 'https://ua1us413q1.execute-api.ap-south-1.amazonaws.com/dev/b2cupdateleadstatus'
    this.http.post(base_URL, {
      id: this.id,
      leadStatus: "Notinterested"
    }).subscribe((data) => {
      if (data['status'] == 200) {
    alert("Hope we Will be able To Serve you Further")
    window.location.reload();
      }
    })
  }
  getCheckListQuestionProdId(event) {
  
    this.calculator(event)
   
  }


  showselectoptions(event) {
    var productValue = $("input[name='typeOfProduct']:checked").val()
    if(productValue == 'Samruddhi') {
      $('.demos').hide();
    } else {
      $('.demos').show();
    }
    
  }
  calculator(event) {
    console.log(this.loan_amount)
    var productValue = $("input[name='typeOfProduct']:checked").val()

    var principalAmt = this.loan_amount;
    var rate = (<HTMLInputElement>document.getElementById("RateOfIntrest")).value;
    var term = $('#NoOfMonths').val();

    if (term != null && principalAmt != "") {
      var t = parseFloat(term);
      var p = parseFloat(principalAmt);
      var r = parseFloat(rate);
      const loanamt = this.form.controls.form2.get('loanamount')
      const nofmnt = this.form.controls.form2.get('tenure')
      var R = (r / (12)) / 100;
      var e = (p * R * (Math.pow((1 + R), t)) / ((Math.pow((1 + R), t)) - 1));

      if (productValue == 'Samriddhi') {

        this.prodIds = '100'

        console.log(this.selectedOption)

        if (p < 51000) {

          alert('You have selected Samriddhi so the loan amount should be in range of 51000-200000 ');
          this.loan_amount = '';
          loanamt.setValidators([Validators.required])

          this.selectedOption = null
          $("#NoOfMonths").prop("selectedIndex", 0);
          nofmnt.setValidators([Validators.required])

          $("#emiAmt").text('Rs:' + 0 + '/-')
          return false

        } else if (p > 200000) {

          alert('You have selected Samriddhi so the loan amount should be in range of 51000-200000');
          this.loan_amount = '';
          loanamt.setValidators([Validators.required])
          this.selectedOption = null
          $("#NoOfMonths").prop("selectedIndex", 0);
          nofmnt.setValidators([Validators.required])
          $("#emiAmt").text('Rs:' + 0 + '/-')
          return false
        } else if (t > 36) {
          alert('You have selected Samriddhi so the No of months should be less than or equal to 36');
          this.loan_amount = '';
          loanamt.setValidators([Validators.required])

          this.selectedOption = null
          $("#NoOfMonths").prop("selectedIndex", 0);
          nofmnt.setValidators([Validators.required])
          $("#emiAmt").text('Rs:' + 0 + '/-')
          return false
        }

      } else if (productValue == 'Samriddhi Plus') {

        this.prodIds = '101'
        if (p < 200001) {
          alert('You have selected Samriddhi Plus so the loan amount should be in range of 200001-500000');
          this.loan_amount = '';
          loanamt.setValidators([Validators.required])

          this.selectedOption = null
          $("#NoOfMonths").prop("selectedIndex", 0);
          nofmnt.setValidators([Validators.required])
          $("#emiAmt").text('Rs:' + 0 + '/-')
          return false
        } else if (p > 500000) {
          alert('You have selected Samriddhi Plus so the loan amount should be in range of 200001-500000');
          this.loan_amount = '';
          loanamt.setValidators([Validators.required])

          this.selectedOption = null
          $("#NoOfMonths").prop("selectedIndex", 0);
          nofmnt.setValidators([Validators.required])
          $("#emiAmt").text('Rs:' + 0 + '/-')
          return false
        }
      } else {

        this.prodIds = '102'
        if (p < 500001) {
          alert('You have selected Super Samriddhi so the loan amount should be in range of 500001-1000000');
          this.loan_amount = '';
          loanamt.setValidators([Validators.required])

          this.selectedOption = null
          $("#NoOfMonths").prop("selectedIndex", 0);
          nofmnt.setValidators([Validators.required])
          $("#emiAmt").text('Rs:' + 0 + '/-')
          return false
        } else if (p > 1000000) {
          alert('You have selected Super Samriddhi so the loan amount should be in range of 500001-1000000');
          this.loan_amount = '';
          loanamt.setValidators([Validators.required])

          this.selectedOption = null
          $("#NoOfMonths").prop("selectedIndex", 0);
          nofmnt.setValidators([Validators.required])
          $("#emiAmt").text('Rs:' + 0 + '/-')
          return false
        }
      }

     
   
     

      $("#emiAmt").text('Rs:' + Math.round(e) + '/-');
      this.emiCount = (Math.round(e))
      $("#emiAmt").append("<span class='rateOfInterst' style='font-size:18px'>" + ' ' + '@' + rate + '%' + '</span>');
      this.eligibilityCheckList(event)
      this.interestedButton(event)
    } else {
      $("#emiAmt").text('Rs:' + 0 + '/-');
    
    }
  }


  selectProduct(event: any) {
    this.selectedProduct = (<HTMLInputElement>document.getElementById("product")).value;
  }
  interestedButton(event) {
    this.loading = true
    this.loanAmount = (<HTMLInputElement>document.getElementById("loanAmount")).value;
    this.roi = (<HTMLInputElement>document.getElementById("RateOfIntrest")).value;
    this.noOfMonths = (<HTMLInputElement>document.getElementById("NoOfMonths")).value;
    this.prodIds = this.prodIds
    this.emi = this.emiCount
    const base_URL = 'https://kfcjqv86sa.execute-api.ap-south-1.amazonaws.com/dev/b2cleadproductmapping'
    this.http.post(base_URL, {
      custid: this.id,
      productType: this.value,
      productId: this.prodIds,
      loanAmount: this.loanAmount,
      rateOfInterest: this.roi,
      noOfMonths: this.noOfMonths,
      emiAmount: this.emiCount
    }).subscribe((data) => {
      console.log(data)

      if (data['status'] == 200) {
        console.log("Successfully Bypassed")

      }
      if (data['status'] == 500) {
        console.log(this.emi)
        $('#notInterested').modal('toggle')
        this.next_step = true;
        console.log(data)

      }
      this.loading = false
    })
  }
  getEligibilityQuestion(event) {
    this.eligibilityCheckList(event)
    this.interestedButton(event)
  }

  submitBasicDetails(event) {
    this.loading = true;
    this.firstName = (<HTMLInputElement>document.getElementById("firstName")).value;
    this.lastName = (<HTMLInputElement>document.getElementById("lastName")).value;
    this.mobileNo = (<HTMLInputElement>document.getElementById("mobileNo")).value;
    this.businessPinCode = (<HTMLInputElement>document.getElementById("bussinesspincode")).value;
    this.businessname = (<HTMLInputElement>document.getElementById("businessname")).value;
    this.gender = (<HTMLInputElement>document.getElementById("gender")).value;

    const base_URL = 'https://j6n4iovh9b.execute-api.ap-south-1.amazonaws.com/dev/leadprofile'

    this.http.post(base_URL, {
      firstname: this.firstName,
      lastname: this.lastName,
      gender: this.gender,
      mobileno: this.mobileNo,
      bussinesspincode: this.businessPinCode,
      businessname: this.businessname

    }).subscribe((data) => {
      console.log(data)
      if (data['status'] == 400) {
        this.responses = data['reason']
        $('#submitModal').modal('toggle')

        this.next_step = false
      } else {

        console.log(data)
        this.productTyp = data['productresponse']
        this.responses = data['reason']
        this.fstName = data['leadProfile'].firstname
        this.id = data['leadProfile'].id
        this.lstName = data['leadProfile'].lastname
        this.mobNo = data['leadProfile'].mobileno
        this.gndr = data['leadProfile'].gender
        this.cibilCheckBox(event)
      }
      this.loading = false;
    })
  }
  termsAndConditions() {
    window.open('assets/images/Terms&conditions.html')
  }
  getProduct(event) {
    this.loading = true;
    this.value = (<HTMLInputElement>document.getElementById("dropdown")).value;
    const base_URL = 'https://kfcjqv86sa.execute-api.ap-south-1.amazonaws.com/dev/b2cproductfetch'

    this.http.post(base_URL, {
      typeOfProducts: this.value,
    }).subscribe((data) => {
      console.log(data)
      this.productTypes = data['productresponse']
      this.loading = false;
    })
  }

  eligibilityCheckList(event) {
    this.loading = true;
    const base_URL = ' https://ozkc5r05ab.execute-api.ap-south-1.amazonaws.com/dev/getchecklist'
    this.http.post(base_URL, {
      productid: this.prodIds,
      language: this.language
    }).subscribe((data) => {

      console.log(this.prodIds)
      this.checkLists = data['eligibilitychecklist']
      this.checkLists.forEach(item => {
        console.log(this.language)
        console.log(this.checkLists)
        if (item.filedtype === 'Radio') {
          let dropdownvalue = item.dropdownvalue
          let dropDownValues = dropdownvalue.split(",")
          let data1 = dropDownValues[0]
          this.dropDownValues = data1
          let data2 = dropDownValues[1]
          this.dropDownValuess = data2
        }
      })
      this.loading = false;



    })
  }

  checklistArr: any = []
  // validateCheckList(event) {
  //   this.checklistArr = []
  //   this.questionslist = []
  //   this.questionslist = $('.slider .active').map(function () {
  //     var data = {
  //       id: $(this).attr('id'),
  //       value: $(this).text()
  //     }
  //     return data;
  //   })
  //     .get();
  //   let dob = (<HTMLInputElement>document.getElementById("date")).value;


  //   var datePipe = new DatePipe('en-US');
  //   this.dob = datePipe.transform(dob, 'dd-MM-yyyy');
  //   this.dobs=datePipe.transform(dob, 'yyyy-MM-dd');

  //   let placeHolder1 = (<HTMLInputElement>document.getElementById("placeHolder1")).value;
  //   let placeHolder2 = (<HTMLInputElement>document.getElementById("placeHolder2")).value;
  //   let groupValue = $("input[name='checkValue']:checked").val()
  //   let placeHolder3 = placeHolder1 + "." + placeHolder2
  //   let placHolder4 = parseFloat(placeHolder3)
  //   this.questionslist.forEach(x => {
  //     this.checklistArr.push('"' + x.id + '":' + '"' + x.value + '"')
  //   })

  //   var z = '{' + '\"' + 1 + '\":\"' + placHolder4 + '\",'
  //   var a = '\"' + 2 + '\":\"' + this.dobs + '\",'
  //   var b = '\"' + 3 + '\":\"' + groupValue + '\",'
  //   var y = z + a + b + this.checklistArr.join(',') + '}'
  //   // console.log(y)
  //   this.loading = true;
  //   const base_URL = 'https://sirzb1t0ui.execute-api.ap-south-1.amazonaws.com/dev/validatechecklist'
  //   this.http.post(base_URL,
  //     {
  //       // customerId: this.id,
  //       // checkListAns: "{\"1\":\"5\",\"2\":\"12-06-1998\",\"3\":\"Individual\",\"4\":\"Yes\",\"5\":\"Yes\",\"6\":\"Yes\",\"7\":\"Yes\",\"8\":\"Yes\",\"9\":\"Yes\",\"10\":\"Yes\",\"11\":\"No\"}",
  //       // productCode: this.prodIds,
  //       customerId: this.id,
  //       checkListAns: y,
  //       productCode: this.prodIds
  //     }
  //   ).subscribe((data) => {
  //     console.log(this.questionslist)
  //     console.log(data)
  //     console.log(this.dob)

  //   })
  // }
  birthyear: any
  currentYear: any
  ageInYears: any
  validateCheckList(event) {
    this.loading = true;
    this.checklistArr = []
    this.questionslist = []
    this.questionslist = $('.slider .active').map(function () {
      var data = {
        orderby: $(this).attr('id'),
        value: $(this).text()
      }
      return data;
    })
      .get();

    let dob = (<HTMLInputElement>document.getElementById("date")).value;
    this.dobs = dob
    var datePipe = new DatePipe('en-US');

    this.dob = datePipe.transform(dob, 'dd-MM-yyyy');
    this.birthyear = datePipe.transform(dob, 'yyyy')
    var today = new Date()
    this.currentYear = datePipe.transform(today, 'yyyy')
    this.ageInYears = this.currentYear - this.birthyear;
    console.log(this.ageInYears)
    let placeHolder1 = (<HTMLInputElement>document.getElementById("placeHolder1")).value;
    let placeHolder2 = (<HTMLInputElement>document.getElementById("placeHolder2")).value;
    let groupValue = $("input[name='checkValue']:checked").val()
    let placeHolder3 = placeHolder1 + "." + placeHolder2
    let placHolder4 = parseFloat(placeHolder3)
    this.questionslist.forEach(x => {
      this.checklistArr.push('"' + x.orderby + '":' + '"' + x.value + '"')
    })

    var z = '{' + '\"' + 9 + '\":\"' + placHolder4 + '\",'
    var a = '\"' + 10 + '\":\"' + this.ageInYears + '\",'
    // var b = '\"' + 3 + '\":\"' + groupValue + '\",'
    var y = z + a + this.checklistArr.join(',') + '}'
    // console.log(y)
    this.loading = true;
    const base_URL = 'https://sirzb1t0ui.execute-api.ap-south-1.amazonaws.com/dev/validatechecklist'
    this.http.post(base_URL,
      {
        // customerId: this.id,
        // checkListAns: "{\"1\":\"5\",\"2\":\"12-06-1998\",\"3\":\"Individual\",\"4\":\"Yes\",\"5\":\"Yes\",\"6\":\"Yes\",\"7\":\"Yes\",\"8\":\"Yes\",\"9\":\"Yes\",\"10\":\"Yes\",\"11\":\"No\"}",
        // productCode: this.prodIds,
        customerId: this.id,
        checkListAns: y,
        productCode: this.prodIds,
      }
    ).subscribe((data) => {
      console.log(this.prodIds)
      console.log(y)
      console.log(data)
      if (data['checklistreponse'] === 'Checklistreject') {
        $('#eligibilitychecklist').modal('toggle')
      } else if (data['status'] === 400 || data['status'] === 500) {
        $('#submitModalsViewRejected').modal('toggle')
      }
      this.loading = false;
    })
  }

  creditBureauCheck(event) {
    this.loading = true
    this.businessAddress = (<HTMLInputElement>document.getElementById("businessAddress")).value;
    this.pan = (<HTMLInputElement>document.getElementById("pan")).value;
    this.dateOfBirth = (<HTMLInputElement>document.getElementById("dateOfBirth")).value;
    this.fatherHusbandName = (<HTMLInputElement>document.getElementById("fatherHusbandName")).value;
    this.relationship = (<HTMLInputElement>document.getElementById("relationship")).value;
    this.states = (<HTMLInputElement>document.getElementById("state")).value;
    this.cities = (<HTMLInputElement>document.getElementById("district")).value;
    this.village = (<HTMLInputElement>document.getElementById("village")).value;
    this.loading = true;
    const base_URL = ' https://knq1avza8b.execute-api.ap-south-1.amazonaws.com/dev/updateleadprofile'

    this.http.post(base_URL, {
      id: this.id,
      firstname: this.fstName,
      lastname: this.lstName,
      mobileno: this.mobNo,
      gender: this.gender,
      businessname: this.businessname,
      bussinesspincode: this.businessPinCode,
      businessaddress: this.businessAddress,
      // panno: this.pan,
      panno: this.panNumber,
      dob: this.dobs,
      father_husband_name: this.fatherHusbandName,
      relationship: this.relationship,
      state: this.states,
      city: this.cities,
      village: this.village,
      address: this.businessAddress


    }).subscribe((data) => {
      console.log(data)
      console.log(this.pan)
      if (data['status'] === 500 || data['status'] === 400) {
        $('#submitModalsViewRejected').modal('toggle')
        this.next_step = true
      } else if (data['status'] === 200 && data['leadProfile'].cibilStatus === 'Cibilapprove') {
        $('#submitModalsViewApproved').modal('toggle')
        // var confettiSettings = { target: 'my-canvas' };
        // var confetti = new ConfettiGenerator(confettiSettings);
        // confetti.render();
        // this.congratsCanvas = true
    

      } else if (data['status'] === 200 && data['leadProfile'].cibilStatus === 'Cibilrejected') {
        $('#submitModalsViewRejected').modal('toggle')
      }
      this.loading = false

      //   this.response = data['cibilStatus']
      //   this.loading = false;
      // if (data['leadProfile'].cibilStatus == 'Approved') {
      //   $('#submitModalsViewApproved').modal('toggle')
      //   this.next_step = true
      // } else {
      //   if (data['leadProfile'].cibilStatus == 'Rejected') {
      //     $('#submitModalsViewRejected').modal('toggle')
      //     this.next_step = true
      //   }
      // }
    })
  }

  cibilCheckBox(event) {
    $('#submitModals').modal('toggle')
    function enableButton() {
      var timer2 = "00:30";

      var interval = setInterval(function () {
        var timer = timer2.split(':');
        //by parsing integer, I avoid all extra string processing
        var minutes = parseInt(timer[0], 10);
        var seconds = parseInt(timer[1], 10);
        --seconds;
        minutes = (seconds < 0) ? --minutes : minutes;
        if (minutes < 0) clearInterval(interval);
        seconds = (seconds < 0) ? 59 : seconds;
        //minutes = (minutes < 10) ?  minutes : minutes;
        $('.countdown').html(minutes + ':' + seconds);
        timer2 = minutes + ':' + seconds;
        if (minutes === 0 && seconds === 0) {
          clearInterval(interval);
          document.getElementById('demo').removeAttribute('disabled');
        }
        else {
          document.getElementById('demo').setAttribute('disabled', this.true);
        }

      }, 1000);
    }



    this.loading = true
    const base_URL = ' https://f94nebmyi7.execute-api.ap-south-1.amazonaws.com/production/otp-sent'

    this.http.post(base_URL, {
      mobileNo: this.mobNo,
      projectName: 'MSME Cust verification',
      projectAuthCode: 'prod123456subk'

    }).subscribe((data) => {

      console.log(data)

      if (data['status'] === 200) {

        document.getElementById('demo').addEventListener('click', function () {
          enableButton();
        });

        enableButton();
      }
      this.response = data['sessionKey']
      this.loading = false;
    })
  }

  resendOtp(event) {
    this.loading = true;
    const base_URL = ' https://f94nebmyi7.execute-api.ap-south-1.amazonaws.com/production/otp-sent'
    this.http.post(base_URL, {
      sessionKey: this.response,
      mobileNo: this.mobNo,
      projectName: 'MSME Cust verification',
      projectAuthCode: 'prod123456subk'

    }).subscribe((data) => {

      if (data['status'] == 200) {
        this.otpValidationsResent = true;
        console.log(data)
        console.log(this.mobNo)
        this.response = data['sessionKey']

      }
      this.loading = false;
    })
  }
  verifyOtp(event) {

    this.otp = (<HTMLInputElement>document.getElementById("otp")).value;
    // $('#submitModals').modal('toggle')
    this.loading = true;
    const base_URL = ' https://f94nebmyi7.execute-api.ap-south-1.amazonaws.com/production/validate-otp'

    this.http.post(base_URL, {
      sessionKey: this.response,
      otp: this.otp,
      mobileNo: this.mobNo,
      projectName: 'MSME Cust verification',
      projectAuthCode: 'prod123456subk'

    }).subscribe((data) => {
      console.log(this.response)
      console.log(this.mobNo)
      console.log(data)
      this.loading = false
      if (data['status'] === 200) {
        (<HTMLInputElement>document.getElementById("otp")).value = ""
        $('#submitModals').modal('toggle')
        $("#NoOfMonths").prop("selectedIndex", 0);

      } else if (data['status'] === 400 || data['status'] === 500) {
        this.otpValidation = true;
        this.next_step = false

      }

    })
  }

  tapHere(event) {
    this.tapHereDetails = []
    var selected_id = event.target.id
    this.productTypes.forEach(data => {
      console.log(selected_id)
      if (selected_id == data.id) {
        this.tapHereDetails.push(data)
      }
    })
  }
  getfunction(event) {
    this.submitBasicDetails(event)
    this.villageApi(event)
  }

  villageApi(event) {
    this.villageList = []
    const base_URL = ' https://bajrdbm0c8.execute-api.ap-south-1.amazonaws.com/dev/b2cvillagemapping'

    this.http.post(base_URL, {
      pincode: this.businessPinCode,

    }).subscribe((data) => {
      this.district = data['villagestatepincode'].districtDesc
      this.state = data['villagestatepincode'].stateDesc
      let state = data['villagestatepincode'].villages[0].villagedesc;
      this.villageList.push(data['villagestatepincode'].villages)
      this.villageList = this.villageList[0]
    })
  }

}




